import React, { Component } from 'react';
import './App.css';
import Navbar from './components/Navbar';
import Banner from './components/Banner';


class App extends Component {
    constructor(){
        super();
        this.state = {
            chatHistory: [
                {
                    text:"Hello Mr. Doe, How may I assist you?",
                    type:"bot-message",
                    widget: null,
                    suggestions:["Loans", "Bank Account", "Credit Cards", "Insurance", "Complaints"]
                }
            ]
        }
    }
    updateChat(newChat){
        let updatedChatHistory = this.state.chatHistory.concat(newChat)
        this.setState(
            {chatHistory:updatedChatHistory}
        )
    }
    render() {
        return (
            <div>
                <Navbar />
                <Banner updateChat={this.updateChat.bind(this)} chats={this.state.chatHistory} />
            </div>
        );
    }
}

export default App;