import React, { Component, Fragment } from 'react';
import guy from './../character.png';
import axios from 'axios';

class Banner extends Component {
    handleUserMessage(e){
        e.preventDefault()
        if(this.refs.message.value){
            let userMessage = this.refs.message.value
            this.refs.message.value = null
            this.getReply(userMessage)
        }
    }

    getReply(userMessage){
        let newChatEntries=[]
        newChatEntries[0] = {
            text:userMessage,
            type:"user-message"
        }
        axios.post('http://localhost:5000/bot', {message: userMessage})
        .then((response)=>{
            newChatEntries.push(response.data)
            this.props.updateChat(newChatEntries.slice())
        });
    }

    widget(widget_type){
        if(widget_type == "balance-cards"){
          return this.props.chats[this.props.chats.length-1].widgetData.map(z => 
            (
              <div className="tile">
                <div className="tile-header">
                    <p className="tile-text">{z.acc_type}</p>
                    <p className="tile-text">{z.account}</p>
                </div>
                <h3 className="currency">{z.balance}</h3>
                <a href="#"><p className="tile-footer">View Transactions</p></a>
              </div>
            )
          )
        }
        else{
            return (
                <div>
                    <div className="side">
                        <img className="side-icon" src={guy} alt=""/>
                        <div className="side-text">
                            <h3>Banker Sharma</h3>
                            <p>Your Personal Banking Assistant</p>
                        </div>
                        <p className="side-content">Banker Sharma is Yellow bank's new chatbot that answers all your queries with regards to your accounts and our services</p>
                    </div>
                    
                </div>
            )
        }
            
    }

    componentDidUpdate(){
        let objDiv = document.getElementById("chatbox-inner");
        objDiv.scrollTop = objDiv.scrollHeight;
    }

    render() {
        return (
            <Fragment>
                <div className="container banner">
                    <div className="row">
                        <div className="col-md-5 chatbox">
                            <div id="chatbox-inner">
                                {this.props.chats.map(x => (
                                    <div className={`${x.type}`}>
                                        { x.type=='bot-message' && (<img className="chat-thumbnail" src={guy}></img>)}
                                        <p className={`chat ${x.type}`}>{x.text}</p>
                                    </div>    
                                ))}
                            </div>
                        </div>
                    <div className="col-md-7">
                        <div className="chat-wrapper">
                            {this.widget(this.props.chats[this.props.chats.length-1].widget)}
                        </div>
                    </div>
                </div>
                <div className="suggestion-wrapper">
                    <div className="suggestions">
                         {this.props.chats[this.props.chats.length-1].suggestions.map(y => 
                            (
                                <button className="btn suggestion" onClick={this.getReply.bind(this,y)}>{y}</button>
                            )
                        )} 
                    </div>
                </div>
            </div>
            <div className="container chat-footer">
                <div className="row">
                    <div className="col-md-12">
                    <form onSubmit={this.handleUserMessage.bind(this)}>
                        <div className="input-group input-section">
                                <input type="text" ref="message" className="form-control" placeholder="Enter your message here"></input>
                                <span className="input-group-btn">
                                    <button className="btn chat-button" type="button submit">Go!</button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
        );
    }
}

export default Banner;